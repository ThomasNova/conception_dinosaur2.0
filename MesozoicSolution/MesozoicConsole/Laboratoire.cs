﻿using System;


namespace Mesozoic
{
    public class Laboratoire
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name,int age=0) where TDinosaur:Dinosaur
         {

             return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name,age);

         }
     }
}
