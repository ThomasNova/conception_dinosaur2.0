using System;
using Mesozoic;
namespace Mesozoic
{
    public class Diplodocus:Dinosaur
    {
       protected override string Specie {get {return "Diplodocus";}}
        public Diplodocus(string name, int age):base(name,"Diplodocus",age)
        {
            
        }
        public override string Roar()
        {
            return "c'est pas possible";
        }
    }
}