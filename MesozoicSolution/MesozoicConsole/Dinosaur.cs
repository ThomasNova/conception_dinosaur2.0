using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string Specie {get {return "Dinosaur";}}
        protected int age;

        public Dinosaur(string name, string Specie, int age)
        {
            this.name = name;
            
            this.age = age;
        }

        public string SayHello()
        {
            return String.Format("Je suis {0}, le {1} j'ai {2} ans.", this.name, Specie, this.age);
        }

        public virtual string Roar()
        {
            return "Grrr";
        }


        public string Hug(Dinosaur dinosaur)
        {
            if (dinosaur == this)
            {
                return String.Format("Je suis {0} et je ne peux pas me faire de câlin à moi-même.", this.name);
            }
            return String.Format("Je suis {0} et je fais un câlin à {1}.", this.name, dinosaur.GetName());
        }


        public string GetName()
        {
            return this.name;
        }

        public string GetSpecie()
        {
            return Specie;
        }

        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string name)
        {
            this.name = name;
        }
  
     

        public void SetAge(int age)
        {
            this.age = age;
        }
        public virtual string ToString(string name, int age)
        {
            return base.ToString();
        }
         public override int GetHashCode()
    {
        int hash = 13;
        hash ^= this.name.GetHashCode();
        hash ^= this.age.GetHashCode();
        hash ^= this.Specie.GetHashCode();
        return hash;
    }
    
    public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
    {
        return dino1.name == dino2.name && dino1.Specie == dino2.Specie && dino1.age == dino2.age;
    }
        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
    {
        return !(dino1 == dino2);
    }

       public override bool Equals(object obj)
    {
        return obj is Dinosaur && this == (Dinosaur)obj;

    }
    }
}