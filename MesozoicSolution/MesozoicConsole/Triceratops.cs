using System;
using Mesozoic;
namespace Mesozoic
{
    public class Triceratops:Dinosaur
    {
        protected override string Specie {get {return "Triceratops";}}
   
        public Triceratops(string name, int age):base(name,"Triceratops", age)
        {

        }
            public override string Roar()
        {
            return "roommm";
        }
    }
}