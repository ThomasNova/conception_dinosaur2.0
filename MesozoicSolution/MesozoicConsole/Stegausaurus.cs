using System;
using Mesozoic;
namespace Mesozoic
{
    public class Stegausaurus : Dinosaur
    {
        
        public Stegausaurus(string name, int age) : base(name, "Stegausaurus",age)
        {

        }
        protected override string Specie { get { return "Stegausaurus"; } }
        public override string Roar()
        {
            return "wizzz";
        }
        public override string ToString()
        {
             return String.Format ("Mesozoic.Stegausaurus = name: {0}, age: {1}", name, age);
        }
    }
}