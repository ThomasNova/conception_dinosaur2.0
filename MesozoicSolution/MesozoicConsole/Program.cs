﻿﻿using System;
using Mesozoic;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            /*Dinosaur louis;

            louis = new Triceratops("Louis", 12);
           
            Dinosaur louis2 = new Stegausaurus("Louis", 12);
            Dinosaur nessie = new Diplodocus("Nessie", 11);
            Console.WriteLine(louis);
            Console.WriteLine(louis.Equals(louis2));
        
            Console.WriteLine(louis == louis2);
            Console.WriteLine(louis.GetHashCode()); 
            Console.WriteLine(louis2.GetHashCode()); 
            Console.WriteLine(nessie.GetHashCode());
           Dinosaur dinosaur = new Stegausaurus("Louis", 12);
            Stegausaurus dinosaur2 = new Stegausaurus("Louis", 12);
            Dinosaur dinosaur3 = new Diplodocus("Louis", 12);
            Console.WriteLine(dinosaur == dinosaur2); 
            Console.WriteLine(dinosaur == dinosaur3);*/
            Diplodocus Nessie = Laboratoire.CreateDinosaur<Diplodocus>("Nessie",11);
            Triceratops Louis = Laboratoire.CreateDinosaur<Triceratops>("Louis",12);
            Console.WriteLine(Nessie.GetAge());
            Console.WriteLine(Nessie.GetName());
            Console.WriteLine(Louis.GetAge());
            Console.WriteLine(Louis.GetName());
            Console.ReadKey();

            /* Console.WriteLine("Présentation de nos dinosaures:");

             Console.WriteLine(louis.SayHello());
             Console.WriteLine(nessie.Roar());

             Console.WriteLine(louis.Hug(nessie));
             Console.WriteLine(nessie.SayHello());

             Console.WriteLine(louis.Hug(louis));
             Console.WriteLine(nessie.Hug(louis));
         */
        }
    }
}
