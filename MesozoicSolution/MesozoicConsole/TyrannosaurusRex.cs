using System;
using Mesozoic;
namespace Mesozoic
{
    public class TyrannosaurusRex:Dinosaur
    {
        protected override string Specie { get { return "TyrannosaurusRex"; } }
        public TyrannosaurusRex(string name, int age):base(name,"TyrannosaurusRex",age)
        {
            
        }
            public override string Roar()
        {
            return "non mais les gars !";
        }
    }
}