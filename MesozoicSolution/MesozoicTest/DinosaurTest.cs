using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
namespace MesozoicTest
{
    public class DinosaurTest
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
           Dinosaur louis = new Triceratops("Louis", 12);
           Dinosaur hervé = new Stegausaurus("Hervé", 13);
           Dinosaur claude = new TyrannosaurusRex("Claude", 15);
            Dinosaur jacky = new Diplodocus("Jacky", 16);

            Assert.Equal("Louis", louis.GetName());
            Assert.Equal(12, louis.GetAge());
            Assert.Equal("Hervé", hervé.GetName());
            Assert.Equal(13, hervé.GetAge());
            Assert.Equal("Claude", claude.GetName());
            Assert.Equal(15, claude.GetAge());
            Assert.Equal("Jacky", jacky.GetName());
            Assert.Equal(16, jacky.GetAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Triceratops("Louis", 12);
            Assert.Equal("roommm", louis.Roar());
            Dinosaur hervé = new Stegausaurus("Hervé", 13);
            Assert.Equal("wizzz", hervé.Roar());
            Dinosaur claude = new TyrannosaurusRex("Claude", 15);
            Assert.Equal("non mais les gars !", claude.Roar());
            Dinosaur jacky = new Diplodocus("Jacky", 16);
            Assert.Equal("c'est pas possible", jacky.Roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
           Dinosaur louis = new Triceratops("Louis", 12);
            Assert.Equal("Je suis Louis, le Triceratops j'ai 12 ans.", louis.SayHello());
            Dinosaur hervé = new Stegausaurus("Hervé", 13);
            Assert.Equal("Je suis Hervé, le Stegausaurus j'ai 13 ans.", hervé.SayHello());
            Dinosaur claude = new TyrannosaurusRex("Claude", 15);
            Assert.Equal("Je suis Claude, le TyrannosaurusRex j'ai 15 ans.", claude.SayHello());
            Dinosaur jacky = new Diplodocus("Jacky", 16);
           Assert.Equal("Je suis Jacky, le Diplodocus j'ai 16 ans.", jacky.SayHello());
        }


        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Triceratops("Louis", 12);
            Dinosaur hervé = new Stegausaurus("Hervé", 13);
            Dinosaur claude = new TyrannosaurusRex("Claude", 15);
            Dinosaur jacky = new Diplodocus("Jacky", 16);

            Assert.Equal("Je suis Louis et je fais un câlin à Hervé.", louis.Hug(hervé));
            Assert.Equal("Je suis Claude et je fais un câlin à Jacky.", claude.Hug(jacky));
            Assert.Equal("Je suis Jacky et je fais un câlin à Claude.", jacky.Hug(claude));
            Assert.Equal("Je suis Hervé et je ne peux pas me faire de câlin à moi-même.", hervé.Hug(hervé));
        }
        [Fact]
        public void TestSurcharge()
        {
            Dinosaur dinosaur = new Stegausaurus("Louis", 12);
            Stegausaurus dinosaur2 = new Stegausaurus("Louis", 12);
           Dinosaur dinosaur3 = new Diplodocus("Louis", 12);
            Assert.Equal(true , dinosaur.Equals(dinosaur2));
            Assert.Equal (false, dinosaur.Equals(dinosaur3));
        }
    

    }
}
