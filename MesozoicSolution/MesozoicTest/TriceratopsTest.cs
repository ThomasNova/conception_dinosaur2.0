using System;
using Xunit;
using Mesozoic;

namespace MesozoicTest
{
    public class TriceratopsTest
    {
        [Fact]
        public void TestTriceratopsConstructor()
        {
            Triceratops olivier = new Triceratops("Olivier", 8);

            Assert.Equal("Olivier", olivier.GetName());
            
            Assert.Equal(8, olivier.GetAge());
        }
        [Fact]
        public void TestTriceratopsRoar()
        {
             Triceratops olivier = new Triceratops("Olivier",8);
            Assert.Equal("roommm", olivier.Roar());
        }
        [Fact]
        public void TestTriceratopsSayHello()
        {
            Triceratops olivier = new Triceratops("Olivier", 8);
            Assert.Equal("Je suis Olivier, le Triceratops j'ai 8 ans.",olivier.SayHello());
        }
        [Fact]
        public void TestTriceratopsHug()
        {
            Triceratops olivier = new Triceratops("Olivier", 8);
            Triceratops nathalie = new Triceratops("Nathalie", 10);

            Assert.Equal("Je suis Olivier et je fais un câlin à Nathalie.", olivier.Hug(nathalie));
            Assert.Equal("Je suis Nathalie et je fais un câlin à Olivier.", nathalie.Hug(olivier));
            Assert.Equal("Je suis Olivier et je ne peux pas me faire de câlin à moi-même.", olivier.Hug(olivier));
        }
    }
}