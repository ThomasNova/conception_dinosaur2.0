using System;
using Xunit;
using Mesozoic;

namespace MesozoicTest
{
    public class DiplodocusTest
    {
        [Fact]
        public void TestDiplodocusConstructor()
        {
            Diplodocus olivier = new Diplodocus("Olivier", 8);

            Assert.Equal("Olivier", olivier.GetName());
            
            Assert.Equal(8, olivier.GetAge());
        }
        [Fact]
        public void TestDiplodocusRoar()
        {
             Diplodocus olivier = new Diplodocus("Olivier",8);
            Assert.Equal("c'est pas possible", olivier.Roar());
        }
        [Fact]
        public void TestDiplodocusSayHello()
        {
            Diplodocus olivier = new Diplodocus("Olivier", 8);
            Assert.Equal("Je suis Olivier, le Diplodocus j'ai 8 ans.",olivier.SayHello());
        }
        [Fact]
        public void TestDiplodocusHug()
        {
            Diplodocus olivier = new Diplodocus("Olivier", 8);
            Diplodocus nathalie = new Diplodocus("Nathalie", 10);

            Assert.Equal("Je suis Olivier et je fais un câlin à Nathalie.", olivier.Hug(nathalie));
            Assert.Equal("Je suis Nathalie et je fais un câlin à Olivier.", nathalie.Hug(olivier));
            Assert.Equal("Je suis Olivier et je ne peux pas me faire de câlin à moi-même.", olivier.Hug(olivier));
        }
    }
}