using System;
using Xunit;
using Mesozoic;

namespace MesozoicTest
{
    public class TyrannosaurusRexTest
    {
        [Fact]
        public void TestTyrannosaurusRexConstructor()
        {
            TyrannosaurusRex olivier = new TyrannosaurusRex("Olivier", 8);

            Assert.Equal("Olivier", olivier.GetName());
            
            Assert.Equal(8, olivier.GetAge());
        }
        [Fact]
        public void TestTyrannosaurusRexRoar()
        {
             TyrannosaurusRex olivier = new TyrannosaurusRex("Olivier",8);
            Assert.Equal("non mais les gars !", olivier.Roar());
        }
        [Fact]
        public void TestTyrannosaurusRexSayHello()
        {
            TyrannosaurusRex olivier = new TyrannosaurusRex("Olivier", 8);
            Assert.Equal("Je suis Olivier, le TyrannosaurusRex j'ai 8 ans.",olivier.SayHello());
        }
        [Fact]
        public void TestTyrannosaurusRexHug()
        {
            TyrannosaurusRex olivier = new TyrannosaurusRex("Olivier", 8);
            TyrannosaurusRex nathalie = new TyrannosaurusRex("Nathalie", 10);

            Assert.Equal("Je suis Olivier et je fais un câlin à Nathalie.", olivier.Hug(nathalie));
            Assert.Equal("Je suis Nathalie et je fais un câlin à Olivier.", nathalie.Hug(olivier));
            Assert.Equal("Je suis Olivier et je ne peux pas me faire de câlin à moi-même.", olivier.Hug(olivier));
        }
    }
}