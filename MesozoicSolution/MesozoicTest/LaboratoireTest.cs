﻿using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;

namespace MesozoicTest
{

    public class LaboratoireTest
    {
        [Fact]
        public void TestCreateDinosaur()
        {
            Diplodocus Louis =  Laboratoire.CreateDinosaur<Diplodocus>("Louis",0);
            Assert.Equal("Louis",Louis.GetName());
            Assert.Equal(0,Louis.GetAge());
        }
    }
}
