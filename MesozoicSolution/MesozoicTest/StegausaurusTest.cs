using System;
using Xunit;
using Mesozoic;

namespace MesozoicTest
{
    public class StegausaurusTest
    {
        [Fact]
        public void TestStegausaurusConstructor()
        {
            Stegausaurus olivier = new Stegausaurus("Olivier", 8);

            Assert.Equal("Olivier", olivier.GetName());
            
            Assert.Equal(8, olivier.GetAge());
        }
        [Fact]
        public void TestStegausaurusRoar()
        {
             Stegausaurus olivier = new Stegausaurus("Olivier",8);
            Assert.Equal("wizzz", olivier.Roar());
        }
        [Fact]
        public void TestStegausaurusSayHello()
        {
            Stegausaurus olivier = new Stegausaurus("Olivier", 8);
            Assert.Equal("Je suis Olivier, le Stegausaurus j'ai 8 ans.",olivier.SayHello());
        }
        [Fact]
        public void TestStegausaurusHug()
        {
            Stegausaurus olivier = new Stegausaurus("Olivier", 8);
            Stegausaurus nathalie = new Stegausaurus("Nathalie", 10);

            Assert.Equal("Je suis Olivier et je fais un câlin à Nathalie.", olivier.Hug(nathalie));
            Assert.Equal("Je suis Nathalie et je fais un câlin à Olivier.", nathalie.Hug(olivier));
            Assert.Equal("Je suis Olivier et je ne peux pas me faire de câlin à moi-même.", olivier.Hug(olivier));
        }
        [Fact]
        public void TestToString()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.Equal("Mesozoic.Stegausaurus = name: Louis, age: 12", louis.ToString());
        }
 
    }
}